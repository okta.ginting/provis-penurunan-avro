# provis penurunan avro



# BUILD THE PROJECT
To build this project execute the following command in command line:
`mvn package`


# RUN THE PROJECT
To run this project execute the following command in command line:
`java -jar target\provis-penurunan-job-0.0.1-SNAPSHOT.jar`

# CUSTOMIZATION
Use `ProvisPenurunanAvroController.java` as starting point

## Custom Query
Put every custom query on the controller's method
Example: `CUSTOM_QUERY_EXAMPLE = "SELECT TO_CHAR(SYSDATE, 'MM-DD-YYYY') as ODATE, DEPTS2.* FROM DEPTS2";`

## CUSTOM SCHEMA/TABLE NAME
put every custom schema table's name on the controller's method
Example : `CUSTOM_TABLE_NAME = "EXAMPLE";

The CUSTOM_TABLE_NAME will be the avro & avsc filename`
. example.avro
. example.AVSC


# APPLICATION PROPERTIES (src/main/resource)
Adjust the following fields :
spring.datasource.url
spring.datasource.username
spring.datasource.password
penurunan.avro.directory
