package com.bca.provis.utilities.avro;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;

public interface ResultSetTransformer {
    byte[][] transform(ResultSet resultSet, String schemaName, String namespace)
                     throws IOException;


}
