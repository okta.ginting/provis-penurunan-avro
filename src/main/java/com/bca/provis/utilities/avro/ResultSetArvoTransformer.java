package com.bca.provis.utilities.avro;

import com.bca.provis.utilities.jdbc.OptionsDefault;
import com.bca.provis.utilities.jdbc.Rows;
import com.bca.provis.utilities.jdbc.RowsAdaptor;
import org.apache.avro.file.CodecFactory;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;

@Component
public class ResultSetArvoTransformer implements ResultSetTransformer {
    @Override
    public byte[][] transform(ResultSet resultSet, String schemaName, String namespace) throws IOException {
        Rows row = new RowsAdaptor(resultSet, new OptionsDefault());
        ResultSetSchemaGenerator.Builder builder = new ResultSetSchemaGenerator.Builder(schemaName, namespace, row);
        ByteArrayOutputStream binaryOutputStream = new ByteArrayOutputStream();

        try (DataFileWriter<GenericRecord> writer = new DataFileWriter<GenericRecord>(
                new GenericDatumWriter<>(builder.schema()))
                .setCodec(CodecFactory.nullCodec())
                .create(builder.schema(), binaryOutputStream)) {
            while (row.next()) {
                writer.append(builder.read(row));
            }
        }
        byte[][] bytes = {
                builder.schema().toString(true).getBytes(), binaryOutputStream.toByteArray()};

        return bytes;
    }
}
