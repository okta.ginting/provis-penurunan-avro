package com.bca.provis.utilities.avro;

import com.bca.provis.utilities.jdbc.DatabaseException;
import com.bca.provis.utilities.jdbc.Row;
import com.bca.provis.utilities.jdbc.SqlArgs;
import org.apache.avro.LogicalType;
import org.apache.avro.LogicalTypes;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Types;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ResultSetSchemaGenerator {
    public static class Builder {
        private String[] names; // column labels
        private final int[] types;
        private final int[] precision;
        private final int[] scale;
        private org.apache.avro.Schema schema;
        private String schemaName;
        private String tableName;
        private final String ODATE = "ODATE";

        public Builder(String schemaName, String tableName, Row r) {
            this.schemaName = schemaName;
            this.tableName = tableName;

            try {
                ResultSetMetaData metadata = r.getMetadata();
                int columnCount = metadata.getColumnCount();
                names = new String[columnCount];
                types = new int[columnCount];
                precision = new int[columnCount];
                scale = new int[columnCount];

                for (int i = 0; i < columnCount; i++) {

                    //For each row fetched by the query
                    //Column names
                    names[i] = metadata.getColumnLabel(i + 1);
                    //Column data types
                    types[i] = metadata.getColumnType(i + 1);

                    //Column precision (applicable only for NUMBER and FLOAT data types)
                    precision[i] = metadata.getPrecision(i + 1);
                    //Column scale (applicable only for NUMBER and FLOAT data types)
                    //metadata.getScale() is always returning -127 for NUMBER and FLOAT columns
                    //Oracle database returns -127 if scale is unspecified for the column
                    //Therefore if the scale is unspecified for the column, set it to the appropriate value based on the table schema
                    scale[i] = metadata.getScale(i + 1);
                    if (scale[i] == -127) {
                        //FLOAT() column in Oracle; therefore no scale specified in the column data type
                        //in GCP BigQuery, the NUMERIC data type is an exact numeric value with 38 digits of precision and 9 decimal digits of scale
                        //Precision is the number of digits that the number contains
                        //Scale is how many of these digits appear after the decimal point
                        precision[i] = 38;
                        scale[i] = 9;
                    }

                    if (scale[i] > 0) {
                        if (scale[i] > 9) {
                            scale[i] = 9;
                        }
                        if ((precision[i] + scale[i]) > 38) {
                            precision[i] = 38;
                            scale[i] = 9;
                        }
                        if (precision[i] - scale[i] > 29) {
                            precision[i] = 29;
                        }
                    } else {
                        if (precision[i] > 29) {
                            precision[i] = 29;
                        }
                    }

                }

                names = SqlArgs.tidyColumnNames(names);
            } catch (SQLException e) {
                throw new DatabaseException("Unable to retrieve metadata from ResultSet", e);
            }
        }

        public org.apache.avro.Schema schema() {

            if (schema == null) {
                List<Schema.Field> fields = new ArrayList<>();

                for (int i = 0; i < names.length; i++) {
                    //For each column in the table
                    //Specify the schema in the AVRO file based on the column data type
                    switch (types[i]) {
                        case Types.TINYINT:
                        case Types.SMALLINT:
                        case Types.INTEGER:
                            fields.add(new org.apache.avro.Schema.Field(names[i],
                                    org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), org.apache.avro.Schema.create(Schema.Type.INT)),
                                    null, Schema.Field.NULL_VALUE));
                            break;
                        case Types.BIGINT:
                            fields.add(new org.apache.avro.Schema.Field(names[i],
                                    org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), org.apache.avro.Schema.create(Schema.Type.LONG)),
                                    null, Schema.Field.NULL_VALUE));
                            break;
                        case Types.REAL:
                        case 100: // Oracle proprietary it seems
                            fields.add(new org.apache.avro.Schema.Field(names[i],
                                    org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), org.apache.avro.Schema.create(Schema.Type.FLOAT)),
                                    null, Schema.Field.NULL_VALUE));
                            break;
                        case Types.DOUBLE:
                        case 101: // Oracle proprietary it seems
                            fields.add(new org.apache.avro.Schema.Field(names[i],
                                    org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), org.apache.avro.Schema.create(Schema.Type.DOUBLE)),
                                    null, Schema.Field.NULL_VALUE));
                            break;
                        case Types.DECIMAL:
                        case Types.NUMERIC:
                            //These are the columns with NUMBER and FLOAT data types in Oracle
                            //Specific data types are:
                            //  FLOAT(126), NUMBER and NUMBER(38) in pediatric side and
                            //  FLOAT(126), NUMBER(18), NUMBER(38), NUMBER(12,2), NUMBER(18,2) in adult side
                            //For pediatric side -->
                            //  PRECISION = 0; SCALE = -127 for NUMBER
                            //  PRECISION = 38; SCALE = 0 for NUMBER(38)
                            //  PRECISION = 38; SCALE = 9 for FLOAT(126)
                            //For adult side -->
                            //  PRECISION = 0; SCALE = -127 for NUMBER
                            //  PRECISION = 18; SCALE = 0 for NUMBER(18)
                            //  PRECISION = 38; SCALE = 0 for NUMBER(38)
                            //  PRECISION = 12; SCALE = 2 for NUMBER(12,2)
                            //  PRECISION = 18; SCALE = 2 for NUMBER(18,2)
                            //  PRECISION = 38; SCALE = 9 for FLOAT(126)
                            //log.warn("\n Column with type NUMERIC = " + names[i] + "; PRECISION = " + precision[i] + "; SCALE = " + scale[i]);
                            if (precision[i] == 0 && scale[i] == -127) {
                                //NUMBER data type in Oracle
                                //This was first set as an INTEGER but later changed it to LONG because of Numeric Overflow exception in Java JDBC
                                //Integer in java is 4 bytes / 32 bits
                                //Long in Java is 8 bytes / 64 bits
                                fields.add(new org.apache.avro.Schema.Field(names[i],
                                        //org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Type.NULL), org.apache.avro.Schema.create(Type.INT)),
                                        org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), org.apache.avro.Schema.create(Schema.Type.LONG)),
                                        null, Schema.Field.NULL_VALUE));
                            } else if (precision[i] != 0 && scale[i] == 0) {
                                //NUMBER(18), NUMBER(38) data types in Oracle
                                //Long in Java is 8 bytes / 64 bits
                                fields.add(new org.apache.avro.Schema.Field(names[i],
                                        org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), org.apache.avro.Schema.create(Schema.Type.LONG)),
                                        null, Schema.Field.NULL_VALUE));
                            } else if (precision[i] != 0 && scale[i] != 0) {
                                //NUMBER(12,2), NUMBER(18,2), FLOAT(126) data types in Oracle
                                //org.apache.avro.Schema bytes = org.apache.avro.Schema.create(Type.BYTES);
                                //bytes.addProp("logical_type", "decimal");
                                //bytes.addProp("precision", precision[i]); //38 or 12 or 18
                                //bytes.addProp("scale", scale[i]); //9 or 2
                                //Setting the above in an alternate way
                                org.apache.avro.Schema bytes = LogicalTypes.decimal(precision[i], scale[i]).addToSchema(org.apache.avro.Schema.create(org.apache.avro.Schema.Type.BYTES));
                                fields.add(new org.apache.avro.Schema.Field(names[i],
                                        org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), bytes),
                                        null, Schema.Field.NULL_VALUE));
                            }
                            break;
                        case Types.BINARY:
                        case Types.VARBINARY:
                        case Types.LONGVARBINARY:
                        case Types.BLOB:
                            fields.add(new org.apache.avro.Schema.Field(names[i],
                                    org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), org.apache.avro.Schema.create(Schema.Type.BYTES)),
                                    null, Schema.Field.NULL_VALUE));
                            break;
                        case Types.CLOB:
                        case Types.NCLOB:
                            fields.add(new org.apache.avro.Schema.Field(names[i],
                                    org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), org.apache.avro.Schema.create(Schema.Type.STRING)),
                                    null, Schema.Field.NULL_VALUE));
                            break;
                        case Types.DATE:
                            org.apache.avro.Schema date = org.apache.avro.Schema.create(Schema.Type.INT);
                            date.addProp(LogicalType.LOGICAL_TYPE_PROP, LogicalTypes.date().getName());
                            fields.add(new org.apache.avro.Schema.Field(names[i],
                                    org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), date),
                                    null, Schema.Field.NULL_VALUE));
                            break;
                        case Types.TIME:
                            org.apache.avro.Schema time = org.apache.avro.Schema.create(Schema.Type.STRING);
                            time.addProp(LogicalType.LOGICAL_TYPE_PROP, LogicalTypes.timeMicros().getName());
                            fields.add(new org.apache.avro.Schema.Field(names[i],
                                    org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), time),
                                    null, Schema.Field.NULL_VALUE));
                            break;
                        case Types.TIMESTAMP:
                            org.apache.avro.Schema timestamp = org.apache.avro.Schema.create(Schema.Type.LONG);
                            timestamp.addProp(LogicalType.LOGICAL_TYPE_PROP, LogicalTypes.timestampMillis().getName());
                            fields.add(new org.apache.avro.Schema.Field(names[i],
                                    org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), timestamp),
                                    null, Schema.Field.NULL_VALUE));
                            break;
                        case Types.NVARCHAR:
                        case Types.VARCHAR:
                        case Types.LONGNVARCHAR:
                        case Types.LONGVARCHAR:
                        case Types.CHAR:
                        case Types.NCHAR:
                            if (precision[i] >= 2147483647) {
                                // Postgres seems to report clobs are varchar(2147483647)
                                fields.add(new org.apache.avro.Schema.Field(names[i],
                                        org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), org.apache.avro.Schema.create(Schema.Type.STRING)),
                                        null, Schema.Field.NULL_VALUE));
                            } else {
                                fields.add(new org.apache.avro.Schema.Field(names[i],
                                        org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), org.apache.avro.Schema.create(Schema.Type.STRING)),
                                        null, Schema.Field.NULL_VALUE));
                            }
                            break;
                        case Types.BIT:
                            fields.add(new org.apache.avro.Schema.Field(names[i],
                                    org.apache.avro.Schema.createUnion(org.apache.avro.Schema.create(Schema.Type.NULL), org.apache.avro.Schema.create(Schema.Type.BOOLEAN)),
                                    null, Schema.Field.NULL_VALUE));
                            break;
                        default:
                            throw new DatabaseException("Don't know how to deal with column type: " + types[i]);
                    }
                }

                schema = org.apache.avro.Schema.createRecord(tableName, null, schemaName, false, fields);
//                schema = addOdate(schema, tableName, schemaName);
            }
            return schema;
        }

        private Schema addOdate(Schema schema, String tableName, String schemaName) {
            List<Schema.Field> originalList = schema.getFields();

            ArrayList<Schema.Field> newList = new ArrayList();

            //create a new "empty" schema
            Schema s2 = Schema.createRecord(tableName, null, schemaName, false);

            //add existing fields
            for(Schema.Field f : originalList) {
                Schema.Field ff = new Schema.Field(f.name(), f.schema(), f.doc(), f.defaultVal());
                newList.add(ff);
            }

            //this here is just to show how to create an optional string, its a union of null and string types
            ArrayList<Schema> optionalString = new ArrayList<>();
            optionalString.add(Schema.create(Schema.Type.NULL));

            Schema timeStampField = Schema.create(Schema.Type.LONG);
            optionalString.add(timeStampField);
            Schema.Field oDateField = new Schema.Field(ODATE, LogicalTypes.timestampMillis().addToSchema(timeStampField), null, null);
            newList.add(oDateField);

            s2.setFields(newList);
            return s2;
        }

        public GenericRecord read(Row r) {
            //FYI - timestamps or dates in Oracle are represented as a long number of milliseconds from the Unix epoch, 1 January 1970 00:00:00.000 UTC
            //FYI - decimals are encoded as a sequence of bytes containing the two's complement representation of the unscaled integer value in big-endian byte order
            //      the decimal fields, in particular, look a bit strange in their JSON representation, but rest assured that the data is stored in full fidelity in the actual AVRO encoding!
            GenericRecord record = new GenericData.Record(schema());
            for (int i = 0; i < names.length; i++) {
                switch (types[i]) {
                    case Types.TINYINT:
                    case Types.SMALLINT:
                    case Types.INTEGER:
                        record.put(names[i], r.getIntegerOrNull());
                        break;
                    case Types.BIGINT:
                        record.put(names[i], r.getLongOrNull());
                        break;
                    case Types.REAL:
                    case 100: // Oracle proprietary it seems
                        record.put(names[i], r.getFloatOrNull());
                        break;
                    case Types.DOUBLE:
                    case 101: // Oracle proprietary it seems
                        record.put(names[i], r.getDoubleOrNull());
                        break;
                    case Types.DECIMAL:
                    case Types.NUMERIC:
                        //These are the columns with NUMBER and FLOAT data types in Oracle
                        //Specific data types are:
                        //  FLOAT(126), NUMBER and NUMBER(38) in pediatric side
                        //  FLOAT(126), NUMBER(18), NUMBER(38), NUMBER(12,2), NUMBER(18,2) in adult side and
                        //For pediatric side -->
                        //  PRECISION = 0; SCALE = -127 for NUMBER
                        //  PRECISION = 38; SCALE = 0 for NUMBER(38)
                        //  PRECISION = 38; SCALE = 9 for FLOAT(126)
                        //For adult side -->
                        //  PRECISION = 0; SCALE = -127 for NUMBER
                        //  PRECISION = 18; SCALE = 0 for NUMBER(18)
                        //  PRECISION = 38; SCALE = 0 for NUMBER(38)
                        //  PRECISION = 12; SCALE = 2 for NUMBER(12,2)
                        //  PRECISION = 18; SCALE = 2 for NUMBER(18,2)
                        //  PRECISION = 38; SCALE = 9 for FLOAT(126)
                        //log.warn("\n Column with type NUMERIC = " + names[i] + "; PRECISION = " + precision[i] + "; SCALE = " + scale[i]);
                        if (precision[i] == 0 && scale[i] == -127) {
                            //NUMBER data type in Oracle
                            //This was first set as an INTEGER but later changed it to LONG because of Numeric Overflow exception in Java JDBC
                            //Integer in java is 4 bytes / 32 bits
                            //Long in Java is 8 bytes / 64 bits
                            //record.put(names[i], r.getIntegerOrNull());
                            record.put(names[i], r.getLongOrNull());
                        } else if (precision[i] != 0 && scale[i] == 0) {
                            //NUMBER(18), NUMBER(38) data types in Oracle
                            //Long in Java is 8 bytes / 64 bits
                            record.put(names[i], r.getLongOrNull());
                        } else if (precision[i] != 0 && scale[i] != 0) {
                            //NUMBER(12,2), NUMBER(18,2), FLOAT(126) data types in Oracle
                            //Use a BigDecimal in Java for these types of columns
                            BigDecimal bigDecimalOrNull = r.getBigDecimalOrNull();
                            if (bigDecimalOrNull == null) {
                                record.put(names[i], null);
                            } else {
                                //Use either RoundingMode.DOWN or RoundingMode.FLOOR to truncate a BigDecimal without rounding
                                bigDecimalOrNull = bigDecimalOrNull.setScale(scale[i], RoundingMode.DOWN);
                                record.put(names[i], ByteBuffer.wrap(bigDecimalOrNull.unscaledValue().toByteArray()));
                            }
                        }
                        break;
                    case Types.BINARY:
                    case Types.VARBINARY:
                    case Types.LONGVARBINARY:
                    case Types.BLOB:
                        byte[] bytesOrNull = r.getBlobBytesOrNull();
                        record.put(names[i], bytesOrNull == null ? null : ByteBuffer.wrap(bytesOrNull));
                        break;
                    case Types.CLOB:
                    case Types.NCLOB:
                        record.put(names[i], r.getClobStringOrNull());
                        break;
                    case Types.DATE:
                        Date dateOrNull = r.getDateOrNull();
                        record.put(names[i], dateOrNull == null ? null :
                                Math.toIntExact(dateOrNull.toInstant()
                                        .atZone(ZoneId.systemDefault())
                                        .toLocalDate().toEpochDay()));
                        break;
                    case Types.TIME:
                        Time sqlTimeOrNull = r.getTimeOrNull();
                        record.put(names[i], sqlTimeOrNull == null ? null : sqlTimeOrNull.toString());
                        break;
                    case Types.TIMESTAMP:
                        Date timeStampOrNull = r.getDateOrNull();
                        record.put(names[i], timeStampOrNull == null ? null : timeStampOrNull.getTime());
                        break;
                    case Types.NVARCHAR:
                    case Types.VARCHAR:
                    case Types.LONGNVARCHAR:
                    case Types.LONGVARCHAR:
                    case Types.CHAR:
                    case Types.NCHAR:
                        if (precision[i] >= 2147483647) {
                            // Postgres seems to report clobs are varchar(2147483647)
                            record.put(names[i], r.getClobStringOrNull());
                        } else {
                            record.put(names[i], r.getStringOrNull());
                        }
                        break;
                    case Types.BIT:
                        record.put(names[i], r.getBooleanOrNull());
                        break;
                    default:
                        throw new DatabaseException("Don't know how to deal with column type: " + types[i]);
                }
            }
//            addOdateValue(record);
            return record;
        }

        private void addOdateValue(GenericRecord record) {
            java.time.Instant instant = Instant.now();

            record.put(ODATE, instant.toEpochMilli());
        }
    }
}
