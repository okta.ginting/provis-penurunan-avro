package com.bca.provis.utilities.jdbc.rewriter;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.util.TablesNamesFinder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class QueryRewriter {
    private static final Logger logger = LogManager.getLogger(QueryRewriter.class);
    private final String expression;

    public QueryRewriter(String expression) {
        this.expression = expression;
    }

    private String addAtFirst(String sql) {
        Statement statement;

        try {
            statement = (Select) CCJSqlParserUtil.parse(sql);
        } catch (Throwable e) {
            logger.error("SQL Expression : " + expression);
            throw new RewriteException("The SQL statement cannot be rewrited!");
        }
        if (!(statement instanceof Select)) {
            throw new RewriteException("The query must be a select query!");
        }
        PlainSelect plainSelect = ((PlainSelect) statement).getPlainSelect();
        List<SelectItem<?>> selectItems = plainSelect.getSelectItems();
        for (SelectItem item : selectItems) {
            System.out.println("OGIN.selectItems : " + item.toString());
        }
        selectItems.add(0, SelectItem.from(new Column(getExpression())));
        plainSelect.setSelectItems(selectItems);

        return plainSelect.toString();
    }

    public String addAtFirstReplaceAsterisk(String sql) {
        Statement statement;

        try {
            statement = (Select) CCJSqlParserUtil.parse(sql);
        } catch (Throwable e) {
            logger.error("SQL Expression : " + expression);
            throw new RewriteException("The SQL statement cannot be rewrited!");
        }
        if (!(statement instanceof Select)) {
            throw new RewriteException("The query must be a select query!");
        }
        PlainSelect plainSelect = ((PlainSelect) statement).getPlainSelect();
        List<SelectItem<?>> selectItems = plainSelect.getSelectItems();
        System.out.println(selectItems.size());

        int size = 0;
        for (SelectItem<?> item : selectItems) {
            if (item.toString().equals("*")) {
                size += 1;
            }
        }

        if(size > 0) {
            /* Replace All Asterik with table name */
            List<SelectItem<?>> replacerTableNameSelectItems = new LinkedList<>();
            TablesNamesFinder tablesNamesFinder = new TablesNamesFinder(); // dapatkan kolom - kolom (ex: 2)
            Set<String> tableList = tablesNamesFinder.getTables(statement);
            for (String tableName : tableList) { // (2x iterasi)
                SelectItem<?> selectItem = new SelectItem(new Column(tableName + ".*"));
                for (SelectItem<?> item : selectItems) {
                    if (item.toString().equals("*")) {
                        replacerTableNameSelectItems.add(selectItem);
                    }
                }
            }

            List<SelectItem<?>> replacerSelectItems = new LinkedList<>(replacerTableNameSelectItems);
            logger.debug("QueryRewriter-asterisk replacer : " + replacerSelectItems.toString());
            plainSelect.setSelectItems(replacerSelectItems);
            /* End Asterisk */
        }
        // Tambahkan SYDATE DI AWAL Expression
        List<SelectItem<?>> selectItems2 = plainSelect.getSelectItems();
        selectItems2.add(0, SelectItem.from(new Column(getExpression())));
        plainSelect.setSelectItems(selectItems2);

        return plainSelect.toString();
    }

    private String addWhereAtLast(String sql) {
        Select statement;
        Expression where;
        try {
            statement = (Select) CCJSqlParserUtil.parse(sql);
            where = CCJSqlParserUtil.parseCondExpression(getExpression());

        } catch (Throwable e) {
            throw new RewriteException("The SQL statement cannot be rewrited!");
        }
        if (!(statement instanceof Select)) {
            throw new RewriteException("The query must be a select query!");
        }
        ((PlainSelect) statement).setWhere(where);

        return statement.toString();
    }

    public String getExpression() {
        return expression;
    }
}
