package com.bca.provis.utilities.jdbc;

public class ConstraintViolationException extends DatabaseException{
    public ConstraintViolationException(String message) {
        super(message);
    }

    public ConstraintViolationException(Throwable cause) {
        super(cause);
    }

    public ConstraintViolationException(String message, Throwable cause) {
        super(message, cause);
    }

}
