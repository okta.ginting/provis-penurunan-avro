package com.bca.provis.utilities.jdbc;

public class DatabaseException extends RuntimeException {
    public DatabaseException(String message) {
        super(message);
    }

    public DatabaseException(Throwable cause) {
        super(cause);
    }

    public DatabaseException(String message, Throwable cause) {
        super(message, cause);
    }
    public static DatabaseException wrap(String message, Throwable cause) {
        if (cause instanceof ConstraintViolationException) {
            return new ConstraintViolationException(message, cause);
        }
        return new DatabaseException(message, cause);
    }

}
