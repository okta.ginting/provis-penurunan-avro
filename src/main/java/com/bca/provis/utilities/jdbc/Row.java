package com.bca.provis.utilities.jdbc;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.sql.ResultSetMetaData;
import java.sql.Time;
import java.time.LocalDate;
import java.util.Date;

public interface Row {
    String[] getColumnLabels();

    ResultSetMetaData getMetadata();

    Boolean getBooleanOrNull();

    Boolean getBooleanOrNull(int columnOneBased);

    Boolean getBooleanOrNull(String columnName);

    boolean getBooleanOrFalse();

    boolean getBooleanOrFalse(int columnOneBased);

    boolean getBooleanOrFalse(String columnName);

    boolean getBooleanOrTrue();

    boolean getBooleanOrTrue(int columnOneBased);

    boolean getBooleanOrTrue(String columnName);

    Integer getIntegerOrNull();

    Integer getIntegerOrNull(int columnOneBased);

    Integer getIntegerOrNull(String columnName);

    int getIntegerOrZero();

    int getIntegerOrZero(int columnOneBased);

    int getIntegerOrZero(String columnName);

    Long getLongOrNull();

    
    Long getLongOrNull(int columnOneBased);

    
    Long getLongOrNull(String columnName);

    long getLongOrZero();

    long getLongOrZero(int columnOneBased);

    long getLongOrZero(String columnName);

    
    Float getFloatOrNull();

    
    Float getFloatOrNull(int columnOneBased);

    
    Float getFloatOrNull(String columnName);

    float getFloatOrZero();

    float getFloatOrZero(int columnOneBased);

    float getFloatOrZero(String columnName);

    
    Double getDoubleOrNull();

    
    Double getDoubleOrNull(int columnOneBased);

    
    Double getDoubleOrNull(String columnName);

    double getDoubleOrZero();

    double getDoubleOrZero(int columnOneBased);

    double getDoubleOrZero(String columnName);
     
    BigDecimal getBigDecimalOrNull();

    
    BigDecimal getBigDecimalOrNull(int columnOneBased);

    
    BigDecimal getBigDecimalOrNull(String columnName);

    
    BigDecimal getBigDecimalOrZero();

    
    BigDecimal getBigDecimalOrZero(int columnOneBased);

    
    BigDecimal getBigDecimalOrZero(String columnName);

    String getStringOrNull();

   String getStringOrNull(int columnOneBased);

    String getStringOrNull(String columnName);

    String getStringOrEmpty();

    String getStringOrEmpty(int columnOneBased);

   String getStringOrEmpty(String columnName);

    String getClobStringOrNull();

    String getClobStringOrNull(int columnOneBased);
    
    String getClobStringOrNull(String columnName);

    String getClobStringOrEmpty();
    
    String getClobStringOrEmpty(int columnOneBased);

    String getClobStringOrEmpty(String columnName);

    Reader getClobReaderOrNull();
   Reader getClobReaderOrNull(int columnOneBased);

    Reader getClobReaderOrNull(String columnName);

    Reader getClobReaderOrEmpty();

    Reader getClobReaderOrEmpty(int columnOneBased);

    Reader getClobReaderOrEmpty(String columnName);
    
    byte[] getBlobBytesOrNull();
    
    byte[] getBlobBytesOrNull(int columnOneBased);

    byte[] getBlobBytesOrNull(String columnName);
    
    byte[] getBlobBytesOrZeroLen();
    
    byte[] getBlobBytesOrZeroLen(int columnOneBased);
    
    byte[] getBlobBytesOrZeroLen(String columnName);
    
    InputStream getBlobInputStreamOrNull();
    
    InputStream getBlobInputStreamOrNull(int columnOneBased);
    
    InputStream getBlobInputStreamOrNull(String columnName);
    
    InputStream getBlobInputStreamOrEmpty();
    
    InputStream getBlobInputStreamOrEmpty(int columnOneBased);
    
    InputStream getBlobInputStreamOrEmpty(String columnName);
    
    Date getDateOrNull();
    
    Time getTimeOrNull();

    Date getDateOrNull(int columnOneBased);
    
    Date getDateOrNull(String columnName);
    
    LocalDate getLocalDateOrNull();
    
    LocalDate getLocalDateOrNull(int columnOneBased);

    LocalDate getLocalDateOrNull(String columnName);

}
