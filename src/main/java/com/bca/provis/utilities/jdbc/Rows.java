package com.bca.provis.utilities.jdbc;

/*
* for reading results from a database query.
*
**/
public interface Rows extends Row{
    boolean next();
}
