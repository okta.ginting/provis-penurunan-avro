package com.bca.provis.utilities.jdbc;

import java.util.Calendar;
import java.util.TimeZone;

public class OptionsDefault implements Options {
    @Override
    public Calendar calendarForTimestamps() {
        return Calendar.getInstance(TimeZone.getDefault());
    }
}
