package com.bca.provis.utilities.jdbc;

import java.util.Calendar;

public interface Options {
    Calendar calendarForTimestamps();
}
