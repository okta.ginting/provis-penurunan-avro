package com.bca.provis.utilities.jdbc.rewriter;

public class RewriteException extends RuntimeException {
    public RewriteException() {
        super();
    }

    public RewriteException(String message) {
        super(message);
    }

    public RewriteException(String message, Throwable cause) {
        super(message, cause);
    }

    public RewriteException(Throwable cause) {
        super(cause);
    }
}
