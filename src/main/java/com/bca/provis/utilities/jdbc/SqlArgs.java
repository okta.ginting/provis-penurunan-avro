package com.bca.provis.utilities.jdbc;

import java.util.LinkedHashSet;
import java.util.Set;

public class SqlArgs {
    public static String[] tidyColumnNames(String[] names) {
        Set<String> uniqueNames = new LinkedHashSet<>();
        for (String name : names) {
            if (name == null || name.length() == 0) {
                name = "column_" + (uniqueNames.size() + 1);
            }
            name = name.replaceAll("[^a-zA-Z0-9]", " ");
            name = name.replaceAll("([a-z])([A-Z])", "$1_$2");
            name = name.trim().toLowerCase();
            name = name.replaceAll("\\s", "_");
            if (Character.isDigit(name.charAt(0))) {
                name = "a" + name;
            }
            int i = 2;
            String uniqueName = name;
            while (uniqueNames.contains(uniqueName)) {
                uniqueName = name + "_" + i++;
            }
            name = uniqueName;
            uniqueNames.add(name);
        }
        return uniqueNames.toArray(new String[uniqueNames.size()]);
    }
}
