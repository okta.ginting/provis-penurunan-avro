package com.bca.provis.utilities.executor;

public class ResultAvro {
    private byte[] asvc;
    private byte[] avro;
    private final String fileName;

    public ResultAvro(byte[][] bytes, String fileName) {
        this.asvc = bytes[0];
        this.avro = bytes[1];
        this.fileName = fileName;
    }

    public byte[] getAsvc() {
        return asvc;
    }

    public void setAsvc(byte[] asvc) {
        this.asvc = asvc;
    }

    public byte[] getAvro() {
        return avro;
    }

    public void setAvro(byte[] avro) {
        this.avro = avro;
    }

    public String getAvscFileName() {
        return fileName + ".avsc";
    }

    public String getAvroFileName() {
        return fileName + ".avro";
    }

    public String getFileName() {
        return fileName;
    }
}
