package com.bca.provis.utilities.executor;

import com.bca.provis.utilities.avro.ResultSetTransformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.*;
import java.sql.*;

@Component
public class ResultSetAvroExecutor implements ResultSetExecutor {
    private final String SEPARATOR = File.separator;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private ResultSetTransformer resultSetTransformer;

    private static final Logger logger = LogManager.getLogger(ResultSetAvroExecutor.class);

    @Override
    public void execute(String query, String directoryOutput, String namespace, String tableName) {
        logger.info("Running Provis penurunan Job@"  + tableName);
        Connection con = null;
        PreparedStatement ps = null;
        Statement statement = null;
        try {
            con = DataSourceUtils.getConnection(this.dataSource);
            statement = con.createStatement();

            ResultSet resultSet = statement.executeQuery(query);
            byte[][] resultBytes = resultSetTransformer.transform(resultSet, tableName, namespace);
            this.dump(directoryOutput, new ResultAvro(resultBytes, tableName));
        } catch (SQLException | IOException e) {
            throw new RuntimeException(e);
        } finally {
            JdbcUtils.closeStatement(ps);
            JdbcUtils.closeStatement(statement);
            DataSourceUtils.releaseConnection(con, this.dataSource);
        }
    }

    private void dump(String directoryOutput, ResultAvro content) {
        ByteArrayInputStream in = new ByteArrayInputStream(content.getAsvc()); // AVSC
        copyInputStreamToFile(in, new File(directoryOutput + this.SEPARATOR + content.getAvscFileName()));
        in = new ByteArrayInputStream(content.getAvro()); // AVRO
        copyInputStreamToFile(in, new File(directoryOutput + this.SEPARATOR + content.getAvroFileName()));
    }

    private void copyInputStreamToFile(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
