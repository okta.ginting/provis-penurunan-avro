package com.bca.provis.utilities.executor;

public interface ResultSetExecutor {
    void execute(String query, String directoryOutput, String namespace, String tableName);
}
