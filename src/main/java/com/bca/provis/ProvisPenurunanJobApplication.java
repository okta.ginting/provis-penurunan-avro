package com.bca.provis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvisPenurunanJobApplication {
	public static final String CUSTOM_QUERY = "";

	public static void main(String[] args) {
		SpringApplication.run(ProvisPenurunanJobApplication.class, args);
	}
}
