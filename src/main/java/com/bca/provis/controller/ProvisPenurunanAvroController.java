package com.bca.provis.controller;

import com.bca.provis.config.ConfigUtility;
import com.bca.provis.utilities.executor.ResultSetExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;


@RestController
public class ProvisPenurunanAvroController {

    @Autowired
    private ConfigUtility configUtil;
    @Autowired
    private ResultSetExecutor executor;

    @GetMapping("/penurunan/example")
    public String dumpExample() {
        final String CUSTOM_QUERY_EXAMPLE = "SELECT TO_CHAR(SYSDATE, 'MM-DD-YYYY') as ODATE, DEPTS2.* FROM DEPTS2";
        final String CUSTOM_TABLE_NAME = "EXAMPLE";

        executor.execute(CUSTOM_QUERY_EXAMPLE, configUtil.getDirectory(), "PROVIS", CUSTOM_TABLE_NAME);
        return "OK";
    }
}
