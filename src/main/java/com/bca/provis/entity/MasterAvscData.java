package com.bca.provis.entity;

import java.util.Date;

public class MasterAvscData {
    private Date createdDate;
    private String avscName;
    private String customQuery;
    private String isNeedODate;

    public MasterAvscData(final Date createdDate, final String avscName, final String customQuery, final String needODate) {
        this.createdDate = createdDate;
        this.avscName = avscName;
        this.customQuery = customQuery;
        this.isNeedODate = needODate;
    }

    public String getAvscName() {
        return avscName;
    }

    public void setAvscName(String avscName) {
        this.avscName = avscName;
    }

    public String getCustomQuery() {
        return customQuery;
    }

    public void setCustomQuery(String customQuery) {
        this.customQuery = customQuery;
    }

    public String getIsNeedODate() {
        return isNeedODate;
    }

    public void setIsNeedODate(String isNeedODate) {
        this.isNeedODate = isNeedODate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
