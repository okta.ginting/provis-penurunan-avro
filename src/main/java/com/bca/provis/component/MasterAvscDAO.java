package com.bca.provis.component;

import com.bca.provis.entity.MasterAvscData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class MasterAvscDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<MasterAvscData> inquiryTableList() {
        String sql = "SELECT created_date, avsc_name, custom_query, need_odate FROM PROVIS_DWH_MASTER_AVSC";
        return this.jdbcTemplate.query(sql, (rs, rowNum) -> {
            return new MasterAvscData(rs.getDate("CREATED_DATE"), rs.getString("AVSC_NAME"), rs.getString("CUSTOM_QUERY"), rs.getString("NEED_ODATE"));
        });
    }

    public List<MasterAvscData> inquiryTableListByAvscName(String avscName) {
        String sql = "SELECT avsc_name, custom_query, need_odate FROM PROVIS_DWH_MASTER_AVSC WHERE avsc_name = ?";
        return this.jdbcTemplate.query(sql, new Object[]{avscName}, (rs, rowNum) -> {
            return new MasterAvscData(rs.getDate("CREATED_DATE"), rs.getString("AVSC_NAME"), rs.getString("CUSTOM_QUERY"), rs.getString("NEED_ODATE"));
        });
    }

    public List<MasterAvscData> inquiryTableListByCreatedDate(int day) {
        String SQL = "SELECT created_date, avsc_name, custom_query, need_odate FROM PROVIS_DWH_MASTER_AVSC WHERE trunc(CREATED_DATE) = trunc(sysdate";
        String SQLResult = extractedDays(day, SQL);
        return this.jdbcTemplate.query(SQLResult, (rs, rowNum) -> {
            return new MasterAvscData(rs.getDate("CREATED_DATE"), rs.getString("AVSC_NAME"), rs.getString("CUSTOM_QUERY"), rs.getString("NEED_ODATE"));
        });
    }

    private String extractedDays(int day, String SQL) {
        StringBuilder builder = new StringBuilder(SQL);
        if (day > 0){
            builder.append(" + ").append(day);
        }
        else if (day < 0) {
            builder.append(" ").append(day);
        }
        builder.append(")");
        return builder.toString();
    }

    private boolean isNullOrZero(Integer num) {
        return Optional.ofNullable(num).orElse(0) == 0;
    }
}
